<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>JFast开发框架 - 登录</title>
    <meta name="description" content="JavaEE企业开发框架 | 登录页面"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <script src="js/jquery.min.js"></script>
    <script>
        function ajax(){
            $.ajax({
                url:"jsp/logout",
                method:"post",
                success:function(respMsg){
                    alert(respMsg.logout);
                }
            });
        }
    </script>
</head>
<p>
<p>
<form action="jsp/login" method="post">
    <table>
        <tr>
            <td>邮箱</td>
            <td><input type="email" name="email"></td>
        </tr>
        <tr>
            <td>密码</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="登陆"></td>
        </tr>
    </table>
</form>
</p>
<p>
<form action="jsp/home.jsp" method="get">
    <input type="submit" value="跳转（jsp后缀）">
</form>
</p>
<p>
<form action="jsp/home.api" method="get">
    <input type="submit" value="跳转（标准api调用）">
</form>
</p>
<p>
    <input type="button" value="注销登录" onclick="ajax()">
</p>

</body>
</html>