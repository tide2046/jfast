package cn.app.wx.task;

import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.web.api.annotation.Schedule;
import cn.jfast.framework.web.api.annotation.ScheduleJob;

@ScheduleJob
public class SimpleTask {

    private Logger log = LogFactory.getLogger(SimpleTask.class);

    @Schedule(cron = "0 1-5 * * * 2016")
    public void run1(){
        log.info("CRON类定时任务启动...");
    }

    @Schedule(delay = 0,repeat = 5,repeatInterval = 500000)
    public void run2(){
        log.info("延时类定时任务启动...");
    }
}
