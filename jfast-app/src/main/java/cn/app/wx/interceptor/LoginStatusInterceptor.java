package cn.app.wx.interceptor;

import cn.jfast.framework.web.aop.AopHandler;
import cn.jfast.framework.web.aop.AopScope;
import cn.jfast.framework.web.aop.annotation.Aop;
import cn.jfast.framework.web.api.ApiInvocation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Aop(scope = AopScope.Global)
public class LoginStatusInterceptor extends AopHandler {

    @Override
    public void beforeHandle(ApiInvocation apiInvocation, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, List<Exception> list) throws Exception {
        apiInvocation.invoke();
    }

    @Override
    public void afterHandle(ApiInvocation apiInvocation, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, List<Exception> list) throws Exception {
        apiInvocation.invoke();
    }
}
