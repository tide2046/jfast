package cn.app.wx.controller;

import cn.app.wx.dao.UserDao;
import cn.app.wx.model.User;
import cn.jfast.framework.base.util.ApiCaller;
import cn.jfast.framework.jdbc.annotation.Transaction;
import cn.jfast.framework.web.api.annotation.*;
import cn.jfast.framework.web.view.Route;
import cn.jfast.framework.web.view.viewtype.Callback;
import cn.jfast.framework.web.view.viewtype.Json;
import cn.jfast.framework.web.view.viewtype.Jsp;
import cn.jfast.framework.web.view.viewtype.Text;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Map;

@Api("/jsp")
public class LoginController {

    @Resource
    UserDao userDao;

    @Transaction
    @Post("login")
    public Json login(String email,String password,HttpServletRequest request) throws SQLException, ClassNotFoundException {
        User user = userDao.selectAllFromUserByEmailPassword(email,password);
        if(null != user) {
            request.getSession().setAttribute("user", user);
            return new Json("{login:true}");
        } else {
            return new Json("{login:false}");
        }
    }

    @Post("home")
    @Get("home")
    public Jsp home(HttpServletRequest request){
        //测试Dao类操作
        Map<String,Object> userMap = userDao.select("*****@**.com");
        boolean user =userDao.deleteFromUserByEmail("****@**.com");
        int result = userDao.updateUserSetPasswordByEmail("******@**.com", "123456");

        if(null != request.getSession().getAttribute("user"))
            return new Jsp("jsp/home", Route.REDIRECT);
        else
            return null;
    }

    @Post("logout")
    public Json logout(HttpServletRequest request){
        ResultDemo resutl = (ResultDemo)ApiCaller.put("jsp/doLogOut",
                request.getSession().getAttribute("user")); //调用jsp/doLogOut服务
        if(resutl.errCode.equals("1"))
            return new Json("{logout:true}");
        else
            return new Json("{logout:false}");
    }

    @Put("doLogOut")
    public Callback put(@RemoteObject User user){ //返回服务回调对象
        if(null != user)
            return new Callback(new ResultDemo("1","注销成功"));
        else
            return new Callback(new ResultDemo("2","未获取到登录账户"));
    }

    class ResultDemo implements Serializable{
        String errCode;
        String errInfo;

        ResultDemo(String errCode,String errInfo){
            this.errCode = errCode;
            this.errInfo = errInfo;
        }
    }

}
