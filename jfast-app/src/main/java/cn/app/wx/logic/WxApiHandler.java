package cn.app.wx.logic;

import cn.jfast.framework.plugin.wx.api.core.WxMsgHandler;
import cn.jfast.framework.plugin.wx.api.vo.OutPutMsg;
import cn.jfast.framework.plugin.wx.api.vo.ReceiveMsg;

/**
 * Wx 消息处理类
 */
public class WxApiHandler extends WxMsgHandler {

    public OutPutMsg def(ReceiveMsg rm) {
        return super.def(rm);
    }

    public OutPutMsg text(ReceiveMsg rm) {
        return super.text(rm);
    }

}
