jfast是javaEE的一款简易mvc框架.

----------


**范例:**

配置web.xml
    

    <?xml version="1.0" encoding="UTF-8"?>

    <web-app version="2.4"
         xmlns="http://java.sun.com/xml/ns/j2ee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">

         <filter>
             <filter-name>JFast</filter-name>
             <filter-class>cn.jfast.framework.web.core.ApiFilter</filter-class>
         </filter>

         <filter-mapping>
            <filter-name>JFast</filter-name>
            <url-pattern>/*</url-pattern>
         </filter-mapping>

         <welcome-file-list>
            <welcome-file>index.jsp</welcome-file>
         </welcome-file-list>

    </web-app>


----------

jfast配置文件(src根目录下)

    <?xml version="1.0" encoding="UTF-8"?>
    
    <context xmlns="http://www.jfast.cn/context"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://www.jfast.cn/context
                                http://www.jfast.cn/context/jfast-context.xsd">

        <!-- 数据库参数配置 -->
        <jdbc jdbcUser="****"
              jdbcPassword="****"
              jdbcUrl="jdbc:mysql://******:3306/jfast?characterEncoding=utf8"
              jdbcDriver="com.mysql.jdbc.Driver"/>

        <!-- 日志属性配置 -->
        <log isInfoEnable="true"
             isWarnEnable="true"
             isDebugEnable="true"
             isErrorEnable="true"/>
        <web isDevMode="true" characterEncoding="UTF-8"/>

        <!-- 微信公众号配置 -->
        <wx aesKey="******"
            appId="******"
            appSecret="******"
            token="******"
            handler="cn.app.wx.logic.WxApiHandler"/>

        <!-- 静态资源配置,现在这个功能还没做 -->
        <resources location="/js" mapping="/js"/>
        <resources location="/image" mapping="/image"/>
    
    </context>

----------

  创建MVC的M模型层

  模型层就是普通的pojo对象,不需要做任何改动.

----------
  
  创建Dao层

    package cn.app.wx.dao;

    import cn.app.wx.model.User;
    import cn.jfast.framework.jdbc.annotation.Body;
    import cn.jfast.framework.jdbc.annotation.Dao;
    import cn.jfast.framework.jdbc.annotation.Select;
    import cn.jfast.framework.jdbc.db.ConnectionFactory;
    import java.sql.Connection;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    java.util.Map;

    @Dao
    public class UserDao {

      private Connection conn;
      private PreparedStatement ps;
      private ResultSet rs;

      @Body  //该方法将执行方法体中的复杂操作(内容可以替换为其他的任何orm框架,实现jfast与其他框架兼容)
      public User selectAllFromUserByEmailPassword(String email, String password) throws SQLException, ClassNotFoundException {
         User user = new User();
         conn = ConnectionFactory.getThreadLocalConnection();
         ps = conn.prepareStatement("select * from user where email = ? and password = ?");
         ps.setString(1,email);
         ps.setString(2,password);
         rs = ps.executeQuery();
         if(rs.next()){
            user.setBirthday(rs.getDate("birthday"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setPhone(rs.getString("phone"));
            user.setSex(rs.getString("sex"));
            user.setUserid(rs.getInt("userid"));
            user.setUsername(rs.getString("username"));
            if(rs.next())
               throw new SQLException("查询目标为一条,但是查出了多条记录.");
         }
         return user;
      }

      /**
      * 像这种没有@Body注解的方法,jfast会解析方法名,参数,返回值,自动生成sql
      */
      public boolean deleteFromUserByEmail(String email){
         return false;
      }

      public int updateUserSetPasswordByEmail(String Email, String password){
         return 1;
      }

      /**
      * 像有Select,Update,Delete,Insert注解的方法,jfast则以注解的sql为准
      */
      @Select(sql = "select * from user where email = :email")
      public Map<String,Object> select(String email){
         return null;
      }

    }

    
    
   jfast对dao的支持与以往的框架不同:
  
    1. Dao类类名需要加@Dao注解,
    2. 如果Dao类类名加@Body注解,那么执行该dao类的方法时,都执行方法体中的内容,比如示例中的第一个方法,执行userDao.selectAllFromUserByEmailPassword(**)时,会使用方法体中的操作;如果没有@Body注解,则jfast会根据方法名以及注解的属性来生成sql
    3. 如果方法名上没有@Select,@Update,@Insert,@Delete中的任何一个注解,那么jfast就根据方法名成来生成sql,具体生成规则:
            update表名Set更改字段By依据字段
            deleteFrom表名By依据字段
            select查询字段From表名By依据字段
            insertInto表名Values插入字段

----------

创建MVC中的C控制器层


    package cn.app.wx.controller;

    import cn.app.wx.dao.UserDao;
    import cn.app.wx.model.User;
    import cn.jfast.framework.base.util.ApiCaller;
    import cn.jfast.framework.jdbc.annotation.Transaction;
    import cn.jfast.framework.web.api.annotation.*;
    import cn.jfast.framework.web.view.Route;
    import cn.jfast.framework.web.view.viewtype.Json;
    import cn.jfast.framework.web.view.viewtype.Jsp;
    import cn.jfast.framework.web.view.viewtype.Text;

    import javax.annotation.Resource;
    import javax.servlet.http.HttpServletRequest;
    import java.sql.SQLException;
    import java.util.Map;

    @Api("/jsp")
    public class LoginController {

        @Resource
        UserDao userDao; //自动注入Dao类,java类名上有@Resource注解时,也可以用@Resource实现注入

        @Transaction  //启动事务管理
        @Post("login")  //接收post请求,请求全路径:/jsp/login
        public Json login(String email,String password,HttpServletRequest request) throws SQLException, ClassNotFoundException {
            User user = userDao.selectAllFromUserByEmailPassword(email,password);
            if(null != user) {
                request.getSession().setAttribute("user", user);
                return new Json("{login:true}");
            } else {
                return new Json("{login:false}");
            }
        }

        @Post("home") //该方法接收Post请求,请求全路径:/jsp/home
        @Get("home")  //该方法也可以接收Get请求,请求全路径:/jsp/home
        public Jsp home(HttpServletRequest request){
            //测试Dao类操作
            Map<String,Object> userMap = userDao.select("*****@**.com");
            boolean user =userDao.deleteFromUserByEmail("****@**.com");
            int result = userDao.updateUserSetPasswordByEmail("******@**.com","123456");

            if(null != request.getSession().getAttribute("user"))
                return new Jsp("jsp/home", Route.REDIRECT);
            else
                return null;
        }

        @Post("logout")
        public Json logout(HttpServletRequest request){
            ResultDemo resutl = (ResultDemo)ApiCaller.put("jsp/doLogOut",
                        request.getSession().getAttribute("user")); //调用jsp/doLogOut服务,返回服务结果对象
            if(resutl.errCode.equals("1"))
                return new Json("{logout:true}");
            else
                return new Json("{logout:false}");
        }

        @Put("doLogOut")
        public Callback put(@RemoteObject User user){ //返回服务回调对象
            if(null != user)
                return new Callback(new ResultDemo("1","注销成功"));
            else
                return new Callback(new ResultDemo("2","未获取到登录账户"));
        }

        class ResultDemo implements Serializable{
            String errCode;
            String errInfo;

            ResultDemo(String errCode,String errInfo){
                this.errCode = errCode;
                this.errInfo = errInfo;
            }
        }

    }



----------

创建拦截器

    package cn.app.wx.interceptor;

    import cn.jfast.framework.web.aop.AopHandler;
    import cn.jfast.framework.web.aop.AopScope;
    import cn.jfast.framework.web.aop.annotation.Aop;
    import cn.jfast.framework.web.api.ApiInvocation;

    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import java.util.List;

    @Aop(scope = AopScope.Global)
    public class LoginStatusInterceptor extends AopHandler {
        @Override
        public void beforeHandle(ApiInvocation apiInvocation, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, List<Exception> list) throws Exception {
            apiInvocation.invoke(); //最后必须调用invoke()方法,否则,请求不会传到指定的Api方法中
        }

        @Override
        public void afterHandle(ApiInvocation apiInvocation, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, List<Exception> list) throws Exception {
            apiInvocation.invoke(); //最后必须调用invoke()方法,否则,后面的拦截器后置方法不会执行
        }
    }


----------

创建定时任务类

    package cn.app.wx.task;

    import cn.jfast.framework.log.LogFactory;
    import cn.jfast.framework.log.Logger;
    import cn.jfast.framework.web.api.annotation.Schedule;
    import cn.jfast.framework.web.api.annotation.ScheduleJob;

    @ScheduleJob
    public class SimpleTask {

        private Logger log = LogFactory.getLogger(SimpleTask.class);

        @Schedule(cron = "0 1-5 * * * 2016")
        public void run1(){
            log.info("CRON类定时任务启动...");
        }

        @Schedule(delay = 0,repeat = 5,repeatInterval = 500000)
        public void run2(){
            log.info("延时类定时任务启动...");
        }
    }

----------

创建JFast单元测试


    package cn.app.wx.dao.test;

    import cn.app.wx.dao.UserDao;
    import cn.app.wx.model.User;
    import cn.jfast.framework.log.LogFactory;
    import cn.jfast.framework.log.Logger;
    import cn.jfast.framework.web.core.junit.JfastJUnit4ClassRunner;
    import org.junit.Test;
    import org.junit.runner.RunWith;
    import javax.annotation.Resource;
    import java.sql.SQLException;

    @RunWith(JfastJUnit4ClassRunner.class)  //jfast项目单元测试,需要配置该注解,启动jfast环境
    public class UserTest {

        @Resource
        private UserDao userDao;

        private Logger log = LogFactory.getLogger(UserTest.class);

        @Test
        public void test() throws SQLException, ClassNotFoundException {
            User user = userDao.selectAllFromUserByEmailPassword("1297450431@qq.com","123456");
            userDao.updateUserSetPasswordByEmail("1297450431@qq.com", "123456");
            userDao.deleteFromUserByEmail("123");
            userDao.select("1297450431@qq.com").get("password");
        }

    }


----------


jfast注解介绍:

   **控制器注解**

   &#64;Api<br>
        其参数为该类对应的请求路径,有该注解的类,在应用启动时,被加载到控制器缓存中.<br>
   &#64;DateFormat<br>
        如果参数对象中包含日期类型或者参数本身就是日期类型,那么参数前需要有该注解,否则,日期参数将不能正确解析.<br>
   &#64;RemoteObject<br>
        使用ApiCaller调用的服务可以传递java对象,目标服务方法中的用来接收远程对象的参数需要有&#64;RemoteObject<br>
        注解 (这里要注意ApiCaller是服务调用,和浏览器发出的请求是不同的)<br>
   &#64;RequireParam<br>
        如果参数名和请求传递到后台的名称不同,那么jfast解析该参数时,<br>
        取@RequireParam注解指定的参数名,如果其属性required为true,则该参数不可为空.<br>
   &#64;Post,<br>
   &#64;Get<br>
        表示该方法只能接受对应方式的请求,其参数值为方法对应的请求路径,<br>
        POST,GET请求可以由浏览器等所有实现http协议的终端发出<br>
   &#64;Put,<br>
   &#64;Delete,<br>
   &#64;Head,<br>
   &#64;Options,<br>
   &#64;Trace<br>
        表示该方法只能接受对应方式的请求,其参数值为方法对应的请求路径,<br>
        PUT,DELETE,HEAD,OPTIONS,TRACE请求可以由ApiCaller或者某些http工具发出.<br>
   &#64;Copy,<br>
   &#64;Connect,<br>
   &#64;Lock,<br>
   &#64;Unlock,<br>
   &#64;Move,<br>
   &#64;Mkcol,<br>
   &#64;Patch,<br>
   &#64;Porpfind,<br>
   &#64;Proppatch,<br>
   &#64;Search<br>
        表示该方法只能接受对应方式的请求,请求可由完整实现http协议的终端发出.<br>
   &#64;Transaction<br>
        声明该方法需要事务管理

   **拦截器注解**

   &#64;Aop<br>
        jfast拦截器类需要继承AopHandler类,并且有&#64;Aop注解<br>
        全局拦截器: Aop注解属性(scope=AopScope.Global) 代表全局拦截器,在应用启动时,被加载到拦截器缓存中,<br>
        方法拦截器: Aop注解属性(scope=AopScope.Method) 代表方法级拦截器(方法拦截器也可以不配置该属性).<br>

   **DAO类注解**

   &#64;Dao<br>
        Dao类声明注解.<br>
   &#64;Select<br>
        select语句注解<br>
   &#64;Update<br>
        update语句注解<br>
   &#64;Delete<br>
        delete语句注解<br>
   &#64;Insert<br>
        insert语句注解<br>
   &#64;Body<br>
        该注解表示,调用该数据接口时,执行该方法的方法体来进行数据操作.否则jfast会解析方法名注解等来执行数据操作<br>

   **定时任务注解**

   &#64;ScheduleJob<br>
        有该注解的类,在应用启动时,被加载到定时任务缓存中.<br>
   &#64;Schedule<br>
        其参数:cron有值时,jfast解析cron表达式来执行定时任务,否则按照 delay(延时多少毫秒启动),<br>
        repeat(任务执行次数),repeatInterval(重复任务的间隔毫秒数) 的规则执行;<br>
        JFast CRON表达式:<br> "* * * * * *" 代表 秒 分 时 日 月 年,<br>其中 每个参数都可以使用 1或者 1,2,3 或者 5-10 或者 1,2-5,7-8,10等形式


----------

jfast控制方法返回视图介绍

   Text视图:返回普通文本给前台<br>
   Jsp视图:返回JSP页面<br>
   Download视图:发送文件给客户端下载,<br>
   Json视图:返回Json数据<br>
   Api视图:转发请求到指定Api(只能携带原请求参数,需要在Api中调用其他Api,且自定义参数时,使用ApiCaller来调用服务)<br>
   Callback:当Api作为远程服务时,返回可序列化对象<br>


