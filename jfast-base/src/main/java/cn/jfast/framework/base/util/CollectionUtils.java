/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.base.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public abstract class CollectionUtils {

	/**
	 * 判断集合是否为空
	 * 
	 * @author Zhuhy
	 * @title : isEmpty
	 * @date 2015年4月17日 下午5:05:43
	 * @param collection
	 * @return boolean
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return (collection == null || collection.isEmpty());
	}

	/**
	 * 判断Map是否为空
	 * 
	 * @author Zhuhy
	 * @title : isEmpty
	 * @date 2015年4月17日 下午5:05:53
	 * @param map
	 * @return boolean
	 */
	public static boolean isEmpty(Map<?, ?> map) {
		return (map == null || map.isEmpty());
	}

	/**
	 * 数组转为List
	 * 
	 * @author Zhuhy
	 * @title : arrayToList
	 * @date 2015年4月17日 下午5:06:19
	 * @param source
	 * @return List
	 */
	@SuppressWarnings("rawtypes")
	public static List arrayToList(Object source) {
		return Arrays.asList(ObjectUtils.toObjectArray(source));
	}

	/**
	 * 把数组装入集合对象
	 * 
	 * @author Zhuhy
	 * @title : mergeArrayIntoCollection
	 * @date 2015年4月17日 下午5:06:47
	 * @param array
	 * @param collection
	 *            void
	 */
	@SuppressWarnings("unchecked")
	public static <E> void mergeArrayIntoCollection(Object array,
			Collection<E> collection) {
		if (collection == null) {
			throw new IllegalArgumentException("Collection must not be null");
		}
		Object[] arr = ObjectUtils.toObjectArray(array);
		for (Object elem : arr) {
			collection.add((E) elem);
		}
	}

	/**
	 * 把属性文件装入Map对象中
	 * 
	 * @author Zhuhy
	 * @title : mergePropertiesIntoMap
	 * @date 2015年4月17日 下午5:07:03
	 * @param props
	 * @param map
	 *            void
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> void mergePropertiesIntoMap(Properties props,
			Map<K, V> map) {
		if (map == null) {
			throw new IllegalArgumentException("Map must not be null");
		}
		if (props != null) {
			for (Enumeration<?> en = props.propertyNames(); en
					.hasMoreElements();) {
				String key = (String) en.nextElement();
				Object value = props.getProperty(key);
				if (value == null) {
					// Potentially a non-String value...
					value = props.get(key);
				}
				map.put((K) key, (V) value);
			}
		}
	}

	/**
	 * 判断集合中是否包含对象
	 * 
	 * @author Zhuhy
	 * @title : containsInstance
	 * @date 2015年4月17日 下午5:08:00
	 * @param collection
	 * @param element
	 * @return boolean
	 */
	public static boolean containsInstance(Collection<?> collection,
			Object element) {
		if (collection != null) {
			for (Object candidate : collection) {
				if (candidate == element) {
					return true;
				}
			}
		}
		return false;
	}

}
