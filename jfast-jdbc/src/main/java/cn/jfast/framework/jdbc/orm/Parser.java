package cn.jfast.framework.jdbc.orm;

import cn.jfast.framework.base.util.StringUtils;

import java.sql.SQLException;

/**
 * 解析预编译格式SQL
 */
public abstract class Parser extends  Wrapper{

    /**
     * 获得 预编译SQl格式
     * @return
     * @throws SQLException
     */
    public String parseSql( ) throws SQLException {
        String sql = getSql();
        String[] sqlParams = StringUtils.substringsBetween(sql, ":", " ");
        for(String sqlParam:sqlParams){
            Object value = paramMaps.get(sqlParam.toLowerCase());
            if(null == value)
                throw new SQLException("解析SQL语句异常：参数 [ "+sqlParam +" ] 不可为空。");
            sqlFields.add(sqlParam);
            sql = sql.replaceAll(":"+sqlParam,"?");
        }
        return sql;
    }

    /**
     * 获得JFast 标准Sql格式
     * @return
     */
    public abstract String getSql();

}
