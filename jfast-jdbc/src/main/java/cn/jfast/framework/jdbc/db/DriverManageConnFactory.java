package cn.jfast.framework.jdbc.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DriverManageConnFactory {

    private Connection conn;
    private static DriverManageConnFactory me;

    public static DriverManageConnFactory me() {
        if (null == me)
            me = new DriverManageConnFactory();
        return me;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DBProperties.JDBC_DRIVER_CLASS);
        conn = DriverManager.getConnection(DBProperties.JDBC_URL, DBProperties.JDBC_USER, DBProperties.JDBC_PASSWORD);
        return conn;
    }

}
