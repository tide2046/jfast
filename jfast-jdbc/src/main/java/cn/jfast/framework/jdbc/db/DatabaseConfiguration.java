package cn.jfast.framework.jdbc.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * 从数据源或者DriverManager中获取数据库链接

 * @author 泛泛o0之辈
 */
public class DatabaseConfiguration {

    private static DataSource dataSource;
	private static Connection conn;

	protected static Connection configure() throws ClassNotFoundException, SQLException {

		if (null == dataSource) {
			try {
				Class.forName("com.alibaba.druid.pool.DruidDataSource");
				AlibabaDruidDataSource druidDataSource = new AlibabaDruidDataSource();
				if (druidDataSource.init()) 
					dataSource = druidDataSource.getDataSource();
			} catch (ClassNotFoundException e) {
				try {
					Class.forName("com.mchange.v2.c3p0.ComboPooledDataSource");
					C3p0DataSource c3poDataSource = new C3p0DataSource();
					if (c3poDataSource.init()) 
						dataSource = c3poDataSource.getDataSource();
				} catch (ClassNotFoundException e1) {
					try {
						Class.forName("org.apache.commons.dbcp.BasicDataSource");
						DbcpDataSource dbcpDataSource = new DbcpDataSource();
						if (dbcpDataSource.init()) 
							dataSource = dbcpDataSource.getDataSource();
					} catch (ClassNotFoundException e2) {
						conn = DriverManageConnFactory.me().getConnection();
					}
				}
			}
		}

		if (null != dataSource)
			conn = dataSource.getConnection();

		return conn;
	}

	protected DataSource getDataSource() {
		return dataSource;
	}

}
