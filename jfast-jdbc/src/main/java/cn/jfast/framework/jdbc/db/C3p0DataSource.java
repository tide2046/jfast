package cn.jfast.framework.jdbc.db;

import java.beans.PropertyVetoException;
import java.sql.SQLException;

import javax.sql.DataSource;


import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.Logger;
import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * C3p0数据源
 *
 * @author 泛泛o0之辈
 */
public class C3p0DataSource implements IDataSource {

	private ComboPooledDataSource dataSource;
	private Logger log = LogFactory.getLogger(C3p0DataSource.class);
	
	/**
	 * 数据源初始化
	 * @return
	 */
	public boolean init() {
		dataSource = new ComboPooledDataSource();
		dataSource.setJdbcUrl(DBProperties.JDBC_URL);
		dataSource.setUser(DBProperties.JDBC_USER);
		dataSource.setPassword(DBProperties.JDBC_PASSWORD);
		try {
			dataSource.setDriverClass(DBProperties.JDBC_DRIVER_CLASS);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		dataSource.setMaxPoolSize(DBProperties.MAX_POOL_SIZE);
		dataSource.setMinPoolSize(DBProperties.MIN_POOL_SIZE);
		dataSource.setInitialPoolSize(DBProperties.INITIAL_POOL_SIZE);
		dataSource.setMaxIdleTime(DBProperties.MAX_IDLE_TIME);
		dataSource.setAcquireIncrement(DBProperties.ACQUIRE_INCREMENT);
		return true;
	}
    
	/**
	 * 获得数据源
	 */
	public DataSource getDataSource() {
		return dataSource;
	}
	
	/**
	 * 关闭数据源
	 * @return
	 * @throws SQLException 
	 */
	public boolean stop() throws SQLException {
		if (dataSource != null)
			dataSource.close();
		return true;
	}
}
