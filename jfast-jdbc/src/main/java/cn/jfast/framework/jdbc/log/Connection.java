package cn.jfast.framework.jdbc.log;

import java.lang.reflect.Proxy;

public class Connection {

	private static java.sql.Connection conn;
	private static ConnectionLogger handler;

	public static java.sql.Connection getConnection(java.sql.Connection con) {
		handler = new ConnectionLogger(con);
		conn = (java.sql.Connection) Proxy.newProxyInstance(con.getClass()
				.getClassLoader(), new Class[]{java.sql.Connection.class}, handler);
		return conn;
	}

}
