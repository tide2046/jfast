package cn.jfast.framework.jdbc.db;

import cn.jfast.framework.jdbc.log.Connection;

import java.sql.SQLException;

/**
 * Connect工厂,获得当前线程的Connection代理对象
 *
 * @author 泛泛o0之辈
 */
public class ConnectionFactory {

    private static final ThreadLocal<java.sql.Connection> threadLocal = new ThreadLocal<java.sql.Connection>();

    /**
     * 获得当前线程下的Connection对象
     *
     * @return
     */
    public static java.sql.Connection getThreadLocalConnection() throws ClassNotFoundException, SQLException {

        java.sql.Connection conn = threadLocal.get();
        if (null == conn || conn.isClosed())
            conn = Connection.getConnection(rebulidDataSource());
        threadLocal.set(conn);
        return conn;

    }

    /**
     * 销毁当前线程的Connection对象
     */
    public static void removeThreadLocalConnection() {
        threadLocal.remove();
    }

    /**
     * 重新初始化数据源
     */
    private static java.sql.Connection rebulidDataSource() throws SQLException, ClassNotFoundException {
        return DatabaseConfiguration.configure();
    }
}
