package cn.jfast.framework.jdbc.db;

import javax.sql.DataSource;

/**
 * 数据源接口
 *
 * @author 泛泛o0之辈
 */
public interface IDataSource {
	DataSource getDataSource();
}
