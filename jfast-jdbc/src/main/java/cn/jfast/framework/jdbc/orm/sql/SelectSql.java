package cn.jfast.framework.jdbc.orm.sql;

import cn.jfast.framework.base.util.ClassUtils;
import cn.jfast.framework.jdbc.annotation.Select;
import cn.jfast.framework.jdbc.annotation.Transaction;
import cn.jfast.framework.jdbc.db.ConnectionFactory;
import cn.jfast.framework.jdbc.orm.Executor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.*;

public  class SelectSql extends Executor{

    private final Select select;
    private ResultSet rs;

    public SelectSql(Type[] paramTypes,
                     String[] paramNames,
                     Object[] args,
                     Select select,
                     Type returnType,
                     Object dao,
                     Method method) throws IllegalAccessException, SQLException, ClassNotFoundException {
        this.dao = dao;
        this.method = method;
        this.paramNames = paramNames;
        this.paramTypes = paramTypes;
        this.args = args;
        this.select = select;
        this.returnType = returnType;
        conn = ConnectionFactory.getThreadLocalConnection();
        super.wrapParam();
    }

    @Override
    public Object execute() throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException {

        Object reObj = null;

        conn.setCatalog(dao.getClass().getSimpleName()+"."+method.getName());
        ps = conn.prepareStatement(parseSql());

        fillPreparedStatement();

        rs = ps.executeQuery();

        ResultSetMetaData meta = rs.getMetaData();
        int column = meta.getColumnCount();
        String[] columns = new String[column];
        int[] columntypes = new int[column];

        for (int i = 0; i < column; i++) {
            columns[i] = meta.getColumnName(i + 1);
            columntypes[i] = meta.getColumnType(i + 1);
        }

        if (returnType instanceof ParameterizedType) {        //如果是带泛型类型的参数
            ParameterizedType parameterizedType = (ParameterizedType) returnType;
            Type basicType = parameterizedType.getRawType();
            Type[] paramTypes = parameterizedType.getActualTypeArguments();

            if (basicType == List.class) {
                Type fType = paramTypes[0];
                Class<?> fClass = (Class<?>) fType;
                List result = new ArrayList();
                while (rs.next()) {
                    if (ClassUtils.isPrimitiveOrWrapper(fClass)
                            || fClass == String.class
                            || fClass == java.util.Date.class
                            || fClass == java.sql.Date.class
                            || fClass == Time.class
                            || fClass == Timestamp.class) {
                        if (column > 1)
                            throw new SQLException("查询语句需要返回单列数据，但是查询了多个列。");
                    }
                    if (fClass == Integer.class) {
                        result.add(rs.getInt(columns[0]));
                    } else if (fClass == Float.class) {
                        result.add(rs.getFloat(columns[0]));
                    } else if (fClass == Double.class) {
                        result.add(rs.getDouble(columns[0]));
                    } else if (fClass == Long.class) {
                        result.add(rs.getLong(columns[0]));
                    } else if (fClass == Boolean.class) {
                        result.add(rs.getBoolean(columns[0]));
                    } else if (fClass == String.class) {
                        result.add(rs.getString(columns[0]));
                    } else if (fClass == Short.class) {
                        result.add(rs.getShort(columns[0]));
                    } else if (fClass == java.util.Date.class) {
                        result.add(rs.getDate(columns[0]));
                    } else if (fClass == java.sql.Date.class) {
                        result.add(rs.getDate(columns[0]));
                    } else if (fClass == Time.class) {
                        result.add(rs.getTime(columns[0]));
                    } else if (fClass == Timestamp.class) {
                        result.add(rs.getTimestamp(columns[0]));
                    } else {
                        Object tempObj = fClass.newInstance();
                        for (int i = 0; i < column; i++) {
                            Field field = fClass.getDeclaredField(columns[i]);
                            if (null != field) {
                                field.setAccessible(true);
                                if (field.getType() == Integer.TYPE || field.getType() == Integer.class)
                                    field.set(tempObj, rs.getInt(columns[i]));
                                else if (field.getType() == Long.TYPE || field.getType() == Long.class)
                                    field.set(tempObj, rs.getLong(columns[i]));
                                else if (field.getType() == Float.TYPE || field.getType() == Float.class)
                                    field.set(tempObj, rs.getFloat(columns[i]));
                                else if (field.getType() == Double.TYPE || field.getType() == Double.class)
                                    field.set(tempObj, rs.getDouble(columns[i]));
                                else if (field.getType() == Boolean.TYPE || field.getType() == Boolean.class)
                                    field.set(tempObj, rs.getBoolean(columns[i]));
                                else if (field.getType() == Short.TYPE || field.getType() == Short.class)
                                    field.set(tempObj, rs.getShort(columns[i]));
                                else if (field.getType() == Byte.TYPE || field.getType() == Byte.class)
                                    field.set(tempObj, rs.getByte(columns[i]));
                                else if (field.getType() == String.class
                                        || field.getType() == Character.class
                                        || field.getType() == Character.TYPE)
                                    field.set(tempObj, rs.getString(columns[i]));
                                else if (field.getType() == java.util.Date.class
                                        || field.getType() == java.sql.Date.class)
                                    field.set(tempObj, rs.getDate(columns[i]));
                                else if (field.getType() == Time.class)
                                    field.set(tempObj, rs.getTime(columns[i]));
                                else if (field.getType() == Timestamp.class)
                                    field.set(tempObj, rs.getTimestamp(columns[i]));
                            }
                        }
                        result.add(tempObj);
                    }
                }
                reObj = result;
            } else if (basicType == Map.class) {
                Type fType1 = paramTypes[0];
                Type fType2 = paramTypes[1];
                if (fType1 != String.class)
                    throw new SQLException("查询语句返回Map时，泛型第一位必须是String。");
                Map result = new HashMap();
                if (rs.next()) {
                    if (fType2 == String.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getString(columns[i]));
                        }
                    else if (fType2 == Integer.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getInt(columns[i]));
                        }
                    else if (fType2 == Long.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getLong(columns[i]));
                        }
                    else if (fType2 == Float.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getFloat(columns[i]));
                        }
                    else if (fType2 == Double.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getDouble(columns[i]));
                        }
                    else if (fType2 == Boolean.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getBoolean(columns[i]));
                        }
                    else if (fType2 == java.util.Date.class
                            ||fType2 == java.sql.Date.class
                            ||fType2 == Time.class
                            ||fType2 == Timestamp.class)
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getDate(columns[i]));
                        }
                    else
                        for (int i = 0; i < column; i++) {
                            result.put(columns[i], rs.getObject(columns[i]));
                        }
                    if (rs.next())
                        throw new SQLException("查询语句需要返回Map对象，要求返回一条记录，但是查询到了多条记录。");
                }
                reObj = result;
            }
        } else {
            if (ClassUtils.isPrimitiveOrWrapper((Class<?>)returnType)
                    || returnType == String.class
                    || returnType == java.util.Date.class
                    || returnType == java.sql.Date.class
                    || returnType == Time.class
                    || returnType == Timestamp.class) {
                if (column > 1)
                    throw new SQLException("查询语句需要返回单列数据，但是查询了多个列。");
                if (rs.next()) {
                    if (returnType == Integer.class)
                        reObj = rs.getInt(columns[0]);
                    else if (returnType == Float.class)
                        reObj = rs.getFloat(columns[0]);
                    else if (returnType == Double.class)
                        reObj = rs.getDouble(columns[0]);
                    else if (returnType == Long.class)
                        reObj = rs.getLong(columns[0]);
                    else if (returnType == Boolean.class)
                        reObj = rs.getBoolean(columns[0]);
                    else if (returnType == String.class)
                        reObj = rs.getString(columns[0]);
                    else if (returnType == Short.class)
                        reObj = rs.getShort(columns[0]);
                    else if (returnType == java.util.Date.class)
                        reObj = rs.getDate(columns[0]);
                    else if (returnType == java.sql.Date.class)
                        reObj = rs.getDate(columns[0]);
                    else if (returnType == Time.class)
                        reObj = rs.getTime(columns[0]);
                    else if (returnType == Timestamp.class)
                        reObj = rs.getTimestamp(columns[0]);
                    if (rs.next())
                        throw new SQLException("查询语句需要返回单个值，要求返回一条记录，但是查询到了多条记录。");
                }
            } else if (returnType == List.class) {
                List result = new ArrayList();
                while (rs.next()) {
                    Map<String, Object> tempMap = new HashMap<String, Object>();
                    for (int i = 0; i < column; i++) {
                        tempMap.put(columns[i], rs.getObject(columns[i]));
                    }
                    result.add(tempMap);
                }
                reObj = result;
            } else if (returnType == Map.class) {
                Map result = new HashMap();
                if (rs.next()) {
                    for (int i = 0; i < column; i++) {
                        result.put(columns[i], rs.getObject(columns[i]));
                    }
                    if (rs.next())
                        throw new SQLException("查询语句需要返回Map对象，要求返回一条记录，但是查询到了多条记录。");
                }
                reObj = result;
            } else {
                Class<?> clazz = (Class<?>) returnType;
                if (rs.next()) {
                    Object result = clazz.newInstance();
                    for (int i = 0; i < column; i++) {
                        Field field = clazz.getDeclaredField(columns[i]);
                        if (null != field) {
                            field.setAccessible(true);
                            if (field.getType() == Integer.TYPE || field.getType() == Integer.class)
                                field.set(result, rs.getInt(columns[i]));
                            else if (field.getType() == Long.TYPE || field.getType() == Long.class)
                                field.set(result, rs.getLong(columns[i]));
                            else if (field.getType() == Float.TYPE || field.getType() == Float.class)
                                field.set(result, rs.getFloat(columns[i]));
                            else if (field.getType() == Double.TYPE || field.getType() == Double.class)
                                field.set(result, rs.getDouble(columns[i]));
                            else if (field.getType() == Boolean.TYPE || field.getType() == Boolean.class)
                                field.set(result, rs.getBoolean(columns[i]));
                            else if (field.getType() == Short.TYPE || field.getType() == Short.class)
                                field.set(result, rs.getShort(columns[i]));
                            else if (field.getType() == Byte.TYPE || field.getType() == Byte.class)
                                field.set(result, rs.getByte(columns[i]));
                            else if (field.getType() == String.class
                                    || field.getType() == Character.class
                                    || field.getType() == Character.TYPE)
                                field.set(result, rs.getString(columns[i]));
                            else if (field.getType() == java.util.Date.class
                                    || field.getType() == java.sql.Date.class)
                                field.set(result, rs.getDate(columns[i]));
                            else if (field.getType() == Time.class)
                                field.set(result, rs.getTime(columns[i]));
                            else if (field.getType() == Timestamp.class)
                                field.set(result, rs.getTimestamp(columns[i]));
                        }
                    }
                    reObj = result;
                }
            }

        }
        if(conn.getAutoCommit() == true && !conn.isClosed())
            conn.close();
        return reObj;
    }

    @Override
    public String getSql() {
        return select.sql()+" ";
    }
}
