package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

import java.io.Serializable;

/**
 * 服务回调视图
 */
public class Callback extends View{
    private Object view;

    public Callback(Object obj){
        if((obj instanceof Serializable))
            this.view = obj;
        else
            throw new IllegalArgumentException("服务回调视图参数必须是可序列化对象,对象需要实现Serializable接口");
    }

    @Override
    public Object getView() {
        return this.view;
    }

    @Override
    public String toString() {
        return "Api返回可序列化对象:" + this.getView();
    }
}
