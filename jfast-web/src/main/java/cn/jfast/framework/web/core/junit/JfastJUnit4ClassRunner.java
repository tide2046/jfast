package cn.jfast.framework.web.core.junit;

import cn.jfast.framework.web.core.ApplicationContext;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

/**
 * Junit测试启动jfast上下文
 */
public class JfastJUnit4ClassRunner extends BlockJUnit4ClassRunner {

    private Class<?> klass;

    private ApplicationContext context = new ApplicationContext() {
        @Override
        public void loadContext() {
            super.loadContext();
        }
    };

    public JfastJUnit4ClassRunner(Class<?> klass) throws InitializationError {
        super(klass);
        this.klass = klass;
    }

    @Override
    protected Statement withBefores(FrameworkMethod method, Object target, Statement statement) {
        context.loadDaoCache();
        context.loadResourceCache();
        target = ApplicationContext.fillResource(target);
        return super.withBefores(method, target, statement);
    }
}
