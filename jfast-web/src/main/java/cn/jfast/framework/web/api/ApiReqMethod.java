/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.api;

/**
 * Http请求方法枚举
 */
public enum ApiReqMethod {

    POST("post"),

    GET("get"),

    PUT("put"),

    DELETE("delete"),

    SEARCH("search"),

    TRACE("trace"),

    OPTIONS("options"),

    MOVE("move"),

    COPY("copy"),

    PATCH("patch"),

    LOCK("lock"),

    UNLOCK("unlock"),

    MKCOL("mkcol"),

    CONNECT("connect"),

    PROPFIND("propfind"),

    PROPPATCH("proppatch"),

    HEAD("head");

    String method;

    ApiReqMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return this.method;
    }

}
