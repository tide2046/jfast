package cn.jfast.framework.plugin.wx.api.web;

import cn.jfast.framework.plugin.wx.api.core.WxBase;
import cn.jfast.framework.plugin.wx.api.core.WxHandler;
import cn.jfast.framework.plugin.wx.api.vo.MPAct;

/**
 * 微信WEB环境交互接口接口
 */
public interface MutualInterface {

    /**
     * 设置微信公众号信息
     *
     * @param mpAct 公众号信息
     */
    void setMpAct(MPAct mpAct);

    /**
     * 设置微信消息处理器
     *
     * @param wxHandler 微信消息处理器
     */
    void setWxHandler(WxHandler wxHandler);

    /**
     * 获取微信基础功能
     *
     * @return 基础功能
     */
    WxBase getWxBase();
}
