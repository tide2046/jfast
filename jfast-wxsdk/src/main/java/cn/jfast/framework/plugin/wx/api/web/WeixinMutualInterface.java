package cn.jfast.framework.plugin.wx.api.web;

import cn.jfast.framework.base.cache.SDKProp;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.plugin.wx.api.core.WxBase;
import cn.jfast.framework.plugin.wx.api.core.WxHandler;
import cn.jfast.framework.plugin.wx.api.vo.MPAct;
import cn.jfast.framework.plugin.wx.crypt.AesException;
import cn.jfast.framework.web.api.annotation.Api;
import cn.jfast.framework.web.api.annotation.Get;
import cn.jfast.framework.web.api.annotation.Post;
import cn.jfast.framework.web.view.viewtype.Text;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Jfast环境接入
 */
@Api("/wx")
public abstract class WeixinMutualInterface implements MutualInterface {

    private static final Logger log = LogFactory.getLogger(LogType.Jfast,WeixinMutualInterface.class);

    // 微信基础功能
    private WxBase wxBase = new WxBase();

    private Text respMsg;


    private void init() throws ServletException {
        MPAct mpAct = MPAct.me();
        mpAct.setAESKey(SDKProp.aesKey);
        mpAct.setAppId(SDKProp.appId);
        mpAct.setAppSecert(SDKProp.appSecret);
        mpAct.setToken(SDKProp.token);
        this.setMpAct(mpAct);
        WxHandler handler = null;
        try {
            handler = (WxHandler)Class.forName(SDKProp.handler).newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        this.setWxHandler(handler);
    }

    /**
     * 处理微信接收URL验证
     *
     * @param req  请求
     * @param resp 响应
     * @throws ServletException
     * @throws IOException
     */
    @Get("/api")
    public Text doGet(HttpServletRequest req,
                         HttpServletResponse resp) throws ServletException, IOException {
        // 响应设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html");
        this.init();
        this.wxBase.init(req);
        try {
            String echo = this.wxBase.check();
            if (!echo.isEmpty()) {
                respMsg = new Text(echo);
            } else {
                log.error("微信接入验证URL时失败!!!");
                log.error("微信服务器echoStr值: %s", this.wxBase.getEchostr());
                log.error("SHA1签名echoStr值: %s", echo);
                respMsg = new Text("error");
            }
        } catch (AesException e) {
            log.error("微信接入验证URL时出现异常!!!");
            log.error(e.getLocalizedMessage(), e);
        }
        return respMsg;
    }

    /**
     * 处理微信普通消息
     *
     * @param req  请求
     * @param resp 响应
     * @throws ServletException
     * @throws IOException
     */
    @Post("api")
    public Text doPost(HttpServletRequest req,
                          HttpServletResponse resp) throws ServletException, IOException {
        this.init();
        this.wxBase.init(req);
        try {
            respMsg = new Text(this.wxBase.handler());
        } catch (Exception e) {
            log.error("解析微信消息时出现异常!!!");
            log.error(e.getLocalizedMessage(), e);
        }
        // 响应设置
        return respMsg;
    }

    public void setMpAct(MPAct mpAct) {
        this.wxBase.setMpAct(mpAct);
    }

    public void setWxHandler(WxHandler wxHandler) {
        this.wxBase.setWxHandler(wxHandler);
    }

    public WxBase getWxBase() {
        return this.wxBase;
    }

}
